const modifiers = [{
  id: 'sugar',
  name: 'Sugar',
  price: 0,
  type: 'taste'
}, {
  id: 'brownsugar',
  name: 'Brown Sugar',
  price: 0.25,
  type: 'taste'
}, {
  id: '2pmilk',
  name: '2% Milk',
  price: 0,
  type: 'dairy'
}, {
  id: 'skim',
  name: 'Skim Milk',
  price: 0.25,
  type: 'dairy'
}, {
  id: 'cream',
  name: 'Cream',
  price: 0.25,
  type: 'dairy'
}, {
  id: 'soymilk',
  name: 'Soy Milk',
  price: 1,
  type: 'non-dairy'
}, {
  id: 'almondmilk',
  name: 'Almond Milk',
  price: 2,
  type: 'non-dairy'
}, {
  id: 'iced',
  name: 'Iced',
  price: 0.75,
  type: 'temp'
}, {
  id: 'chocosyrup',
  name: 'Chocolate Syrup',
  price: 0,
  type: 'syrup'
}, {
  id: 'caramelsyrup',
  name: 'Camarel Syrup',
  price: 0,
  type: 'syrup'
}, {
  id: 'eggnogsyrup',
  name: 'Eggnog Syrup',
  price: 1,
  type: 'syrup'
}, {
  id: 'gingerbsyrup',
  name: 'Gingerbread Syrup',
  price: 0.25,
  type: 'syrup'
}, {
  id: 'whip',
  name: 'Whip Cream',
  price: 0,
  type: 'topping'
}, {
  id: 'darkchoccurl',
  name: 'Dark Chocolate Curl',
  price: 0.5,
  type: 'topping'
}, {
  id: 'whitechoccurl',
  name: 'White Chocolate Curl',
  price: 0.5,
  type: 'topping'
}, {
  id: 'mango',
  name: 'Mango',
  price: 0,
  type: 'fruit'
}, {
  id: 'banana',
  name: 'Banana',
  price: 2,
  type: 'fruit'
}, {
  id: 'ginger',
  name: 'Ginger',
  price: 2,
  type: 'fruit'
}, {
  id: 'orange',
  name: 'Orange',
  price: 0,
  type: 'fruit'
}, {
  id: 'blueberry',
  name: 'Blueberry',
  price: 0.75,
  type: 'fruit'
}, {
  id: 'mixburry',
  name: 'Mixed-Berry',
  price: 0.5,
  type: 'fruit'
}, {
  id: 'strawberry',
  name: 'Strawberry',
  price: 0.25,
  type: 'fruit'
}, {
  id: 'kiwi',
  name: 'Kiwi',
  price: 0.75,
  type: 'fruit'
}, {
  id: 'papaya',
  name: 'Papaya',
  price: 0.5,
  type: 'fruit'
}, {
  id: 'peach',
  name: 'Peach',
  price: 0.5,
  type: 'fruit'
}]

var ModifierApi = function () {
}

ModifierApi.prototype.getByType = function (types, cb) {
  setTimeout(function () {
    var result

    if (types) {
      if (typeof types === 'string') {
        types = [types]
      }
      result = modifiers.filter(function (modifier) {
        return types.indexOf(modifier.type) !== -1
      })
    }

    // doing deep clone here to mimic actual result from http call
    result = JSON.parse(JSON.stringify(result))
    cb(result)
  }, 500)
}

ModifierApi.prototype.get = function (cb, id) {
  setTimeout(function () {
    var result

    if (id) {
      result = modifiers.find(function (modifier) {
        return modifier.id === id
      })
    }
    cb(result)
  }, 500)
}

ModifierApi.prototype.getAll = function (cb) {
  setTimeout(function () {
    // doing deep clone here to mimic actual result from http call
    var result = JSON.parse(JSON.stringify(modifiers))
    cb(result)
  }, 500)
}

export default ModifierApi

