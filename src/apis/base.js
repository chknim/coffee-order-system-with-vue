const bases = [{
  id: 'dripcoffee',
  name: 'Drip Coffee',
  price: 2,
  modifierSpecs: [{
    name: 'Sweetness',
    types: ['taste'],
    canChoosePortion: true,
    maxPortion: 2,
    canMix: false,
    isRequired: false
  }, {
    name: 'Dairy & Non-Dairy',
    types: ['dairy', 'non-dairy'],
    canChoosePortion: true,
    canMix: false, // no milk with almond milk
    isRequired: false
  }, {
    name: 'Hot / Cold',
    types: ['temp'],
    canChoosePortion: false,
    canMix: false,
    isRequired: false
  }]
}, {
  id: 'latte',
  name: 'Latte',
  price: 3,
  modifierSpecs: [{
    name: 'Dairy & Non-Dairy',
    types: ['dairy', 'non-dairy'],
    canChoosePortion: false,
    canMix: false, // no milk with almond milk
    isRequired: true
  }, {
    name: 'Flavor',
    types: ['syrup'],
    canChoosePortion: false,
    canMix: false,
    isRequired: false
  }, {
    name: 'Toppings',
    types: ['topping'],
    canChoosePortion: false,
    canMix: true, // mix whip cream with chocolate curl
    maxMix: 2,
    isRequired: false
  }]
}, {
  id: 'smoothie',
  name: 'Smoothie',
  price: 1.5,
  modifierSpecs: [{
    name: 'Flavor',
    types: ['fruit'],
    canChoosePortion: false,
    canMix: true,
    maxMix: 3,
    isRequired: true
  }]
}, {
  id: 'tea',
  name: 'Tea',
  price: 2.5,
  modifierSpecs: [{
    name: 'Sweetness',
    types: ['taste'],
    canChoosePortion: true,
    maxPortion: 2,
    canMix: false,
    isRequired: false
  }, {
    name: 'Dairy',
    types: ['dairy'],
    canChoosePortion: false,
    canMix: false, // no mix of 2% and skim
    isRequired: false
  }, {
    name: 'Hot / Cold',
    types: ['temp'],
    canChoosePortion: false,
    canMix: false,
    isRequired: false
  }]
}]

var BaseApi = function () {
}

BaseApi.prototype.get = function (cb, id) {
  setTimeout(function () {
    var result

    if (id) {
      result = bases.find(function (base) {
        return base.id === id
      })
    }
    cb(result)
  }, 500)
}

BaseApi.prototype.getAll = function (cb) {
  setTimeout(function () {
    // doing deep clone here to mimic actual result from http call
    var result = JSON.parse(JSON.stringify(bases))
    cb(result)
  }, 500)
}

export default BaseApi

