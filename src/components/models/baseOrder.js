/*
  Order is a composition of specification and product
  Two inheritances are drink order and modifier order
  Drink order is a composition of specification, product and a list of modifier orders
*/
var BaseOrder = function (spec, product) {
  this.spec = spec
  this.product = product
}

BaseOrder.prototype.isRequired = function () {
  return this.spec.isRequired
}

BaseOrder.prototype.isValid = function () {
  return true
}

BaseOrder.prototype.hasValue = function () {
  return true
}

BaseOrder.prototype.getName = function () {
  return this.product.name
}

BaseOrder.prototype.getPrice = function () {
  return this.product.price
}

BaseOrder.prototype.getTotalPrice = function () {
  return this.product.price
}

export default BaseOrder
