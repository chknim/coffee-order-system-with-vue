import { default as BaseOrder } from './baseOrder'
import { default as ModifierProduct } from './modifierProduct'

/*
  Modifier Order is a composition of specification and one modifier product
*/
var ModifierOrder = function (spec, modifierProduct) {
  if (spec instanceof ModifierOrder) {
    var copyObj = spec
    BaseOrder.call(this,
      JSON.parse(JSON.stringify(copyObj.spec)), new ModifierProduct(copyObj.product))

    if (copyObj.selections) {
      this.selections = JSON.parse(JSON.stringify(copyObj.selections))
    }

    this.maxSelection = copyObj.spec.maxMix

    if (copyObj.selection) {
      this.selections = JSON.parse(JSON.stringify(copyObj.selection))
    }

    this.portion = copyObj.portion
    this.maxPortion = copyObj.maxPortion
  } else {
    BaseOrder.call(this, spec, new ModifierProduct(modifierProduct))

    if (spec.canMix) {
      this.selections = []
      if (spec.maxMix) {
        this.maxSelection = spec.maxMix
      }
    } else {
      this.selection = ''
    }

    if (spec.canChoosePortion) {
      this.portion = 0
      if (spec.maxPortion) {
        this.maxPortion = spec.maxPortion
      }
    }
  }
}

ModifierOrder.prototype = Object.create(BaseOrder.prototype)
ModifierOrder.prototype.constructor = ModifierOrder

ModifierOrder.prototype.incrementPortion = function () {
  if (!this.maxPortion || (this.portion + 1 <= this.maxPortion)) {
    this.portion++
  }
}

ModifierOrder.prototype.decrementPortion = function () {
  if (this.portion > 0 && this.portion - 1 >= 0) {
    this.portion--
  }
}

ModifierOrder.prototype.reset = function () {
  if (this.selections) {
    this.selections = []
  }

  if (this.selection) {
    this.selection = ''
  }

  if (this.portion) {
    this.portion = 0
  }
}

ModifierOrder.prototype.isValid = function () {
  if (this.isRequired()) {
    return this.hasValue()
  }
  return true
}

ModifierOrder.prototype.hasValue = function () {
  return (this.selections && this.selections.length) || !!this.selection
}

ModifierOrder.prototype.getOrder = function () {
  return this.selections || this.selection
}

ModifierOrder.prototype.getTotalPrice = function () {
  var price = BaseOrder.prototype.getTotalPrice.call(this)
  if (this.canChoosePortion()) {
    price *= this.getPortion()
  }
  return price
}

ModifierOrder.prototype.canChoosePortion = function () {
  return this.spec.canChoosePortion
}

ModifierOrder.prototype.getPortion = function () {
  return this.portion
}

export default ModifierOrder
