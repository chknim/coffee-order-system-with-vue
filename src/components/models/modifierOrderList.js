import { default as ModifierOrder } from './modifierOrder'

/*
  Modifier Order List contains a list of modifier orders
*/
var ModifierOrderList = function (modifierOrders) {
  this.orders = []
  var that = this
  for (var orderType in modifierOrders) {
    modifierOrders[orderType].forEach(function (order) {
      if (order.hasValue()) {
        that.orders.push(new ModifierOrder(order))
      }
    })
  }
}

export default ModifierOrderList
