import { default as BaseOrder } from './baseOrder'
import { default as ModifierOrderList } from './modifierOrderList'

/*
  A drink order is a composition of spec, product, and a list of modifier orders
*/
var DrinkOrder = function (spec, product, modifierOrderListByType) {
  BaseOrder.call(this, spec, product)
  this.modifierOrderList = new ModifierOrderList(modifierOrderListByType)
}

DrinkOrder.prototype = Object.create(BaseOrder.prototype)
DrinkOrder.prototype.constructor = DrinkOrder

DrinkOrder.prototype.getTotalPrice = function () {
  var totalPrice = 0
  totalPrice += BaseOrder.prototype.getTotalPrice.call(this)
  this.modifierOrderList.orders.forEach(function (modifierOrder) {
    totalPrice += modifierOrder.getTotalPrice()
  })
  return totalPrice
}

export default DrinkOrder
