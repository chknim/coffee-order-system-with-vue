import { default as BaseProduct } from './baseProduct'

/*
  Modifier product is a product plus type
*/
var ModifierProduct = function (modifier) {
  BaseProduct.call(this, modifier)
  this.type = modifier.type
}

ModifierProduct.prototype = Object.create(BaseProduct.prototype)
ModifierProduct.prototype.constructor = ModifierProduct

export default ModifierProduct
