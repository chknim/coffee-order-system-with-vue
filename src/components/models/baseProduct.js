/*
  A product is a composition of id, name, and price
*/
var BaseProduct = function (product) {
  this.id = product.id
  this.name = product.name
  this.price = product.price
}

BaseProduct.prototype.getPrice = function () {
  return this.price
}

BaseProduct.prototype.getName = function () {
  return this.name
}

export default BaseProduct
