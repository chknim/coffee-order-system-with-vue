# Coffee Ordering System with VueJs
Owning a coffee shop (Chang Coffee Shop) is something that would be nice for a side business. To prepare for that dream, I need to build a point of sale system that fits my own demands. Hence this front-end for drink ordering system implemented with Vue.js. OOP is applied into the ordering system and UI view. The application is also built with responsiveness allowing the system to be used via a desktop computer or a handheld device.

## Start it up

Note: Tested on Node.js 6.9.1.

```
npm install or yarn
```

Start development server:

```
npm run dev or yarn run dev
```
